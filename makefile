CC =		gcc

CLIBS =		$(shell sdl-config --libs) -lSGE -lSDL_ttf -lfreetype -lSDL_mixer -lSDL_gfx
CFLAGS =	-O3 -Wall $(shell sdl-config --cflags)

WINCC =		i586-mingw32msvc-gcc -O3 -Wall
WINCLIBS =		-I/usr/i586-mingw32msvc/include -I/usr/i586-mingw32msvc/include/SDL -D_GNU_SOURCE=1 -Dmain=SDL_main -L/usr/i586-mingw32msvc/lib -lmingw32 -lSDLmain -lSDL -lSGE -lSDL_ttf -lSDL_mixer -lSDL_gfx -mwindows

SOURCES =	tripic.c tripic.h

all:		tripic

clean:
		rm tripic

tripic:	${SOURCES}
		${CC} ${CFLAGS} -o tripic ${SOURCES} ${CLIBS}

win:	${SOURCES}
		${WINCC} -o tripic.exe ${SOURCES} ${WINCLIBS}

