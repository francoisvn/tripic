/*
  ------------------------------------------
  tripic
  ------------------------------------------
  by francois van niekerk
  ------------------------------------------
*/

#include "tripic.h"

#define TITLE "tripic (alpha 0.01)"
#define EVENT_THREAD 1
#define MAX_FPS 20
#define DRAW_DIFF 1

#define TRI_NUM 100
#define TRI_VERT 100
#define TRI_INIT_VERT 3
#define TRI_INIT_RND 0
#define TRI_INIT_A 0.5
#define TRI_WHOLE_RND 100
#define TRI_REMOVE_RND 100
#define TRI_ADD_TRI_LVL 50
#define TRI_ADD_TRI_FACTOR 2
#define TRI_ADD_VERT_LVL 50
#define TRI_ADD_VERT_FACTOR 3
#define TRI_ADD_VERT_RND 20
#define TRI_REMOVE_VERT_LVL 100
#define TRI_REMOVE_VERT_FACTOR 0
#define TRI_REMOVE_VERT_RND 30
#define TRI_MOVE_TRI_RND 50
#define TRI_ADD_XY_DELTA 0.2
#define TRI_XY_DELTA 0.2
#define TRI_C_DELTA 50
#define TRI_XY_C_FACTOR 2

//#define PIC_FILENAME "pics/monalisa.png"
//#define PIC_FILENAME "pics/darwin.png"
//#define PIC_FILENAME "pics/velociraptor.png"
//#define PIC_FILENAME "pics/johnkoza.png"
//#define PIC_FILENAME "pics/firefox.png"
//#define PIC_FILENAME "pics/waycool.png"
//#define PIC_FILENAME "pics/tux.png"
//#define PIC_FILENAME "pics/circle.png"
//#define PIC_FILENAME "pics/rocks.png"
//#define PIC_FILENAME "pics/trees.png"
//#define PIC_FILENAME "pics/waterfall.png"
//#define PIC_FILENAME "pics/linus.png"
#define PIC_FILENAME "pics/monalisa2.png"

#define STATE_FILE "data/state.dat"

#define TEXT_FONT "mgopenmodata.ttf"
#define TEXT_SIZE 14
#define TEXT_FPS_ACC 10

struct TRI
{
  int active;
  int points;
  int r;
  int g;
  int b;
  int a;
  Sint16 x[TRI_VERT];
  Sint16 y[TRI_VERT];
};
typedef struct TRI TRI;

SDL_Surface *screen;
SDL_Surface	*pic,*tripic;
TTF_Font *font;
Uint32 bgcolor;

int running;
int pause;
int ticks;
int ticks_delta;
float fps, fps_hist[TEXT_FPS_ACC];

TRI tris [TRI_NUM];
int triw;
int trih;
int activetri;
int pointstri;
int mutations;
int wastemutations;
int benefitial;
long fitness;
long init_fitness;

long comp_fitness(SDL_Surface	*tp)
{
  register long fit;
  register int x,y;
  Uint8 pr,pg,pb;
  Uint8 tr,tg,tb;
  
  fit=0;
  
  for (x=0;x<triw;x++)
  {
    for (y=0;y<trih;y++)
    {
      SDL_GetRGB(GetPixel(pic,x,y),screen->format,&pr,&pg,&pb);
      SDL_GetRGB(GetPixel(tp,x,y),screen->format,&tr,&tg,&tb);
      
      fit+=abs(pr-tr)+abs(pg-tg)+abs(pb-tb);
    }
  }
  
  return fit;
}

void do_step()
{
  register int i,j;
  register TRI *t;
  int ni,nj,nk;
  Sint16 nx,ny;
  TRI nt;
  long nfit;
  SDL_Surface	*tp;
  SDL_Surface	*tmp_surface;
  
  ni=rand()/((double)RAND_MAX+1)*TRI_NUM;
  nk=ni;
  nt=tris[ni];
  if (nt.active)
  {
    if ((nt.points<TRI_VERT) && (wastemutations>=(TRI_ADD_VERT_LVL+pointstri*TRI_ADD_VERT_FACTOR/activetri)) && ((int)(rand()/((double)RAND_MAX+1)*TRI_ADD_VERT_RND)==0))
    {
      nj=rand()/((double)RAND_MAX+1)*(nt.points);
      for (j=(nt.points);j>(nj);j--)
      {
        nt.x[j]=nt.x[j-1];
        nt.y[j]=nt.y[j-1];
      }
      if (nj==nt.points)
      {
        nx=(nt.x[nj-1]+nt.x[0])/2;
        ny=(nt.y[nj-1]+nt.y[0])/2;
      }
      else
      {
        nx=(nt.x[nj-1]+nt.x[nj+1])/2;
        ny=(nt.y[nj-1]+nt.y[nj+1])/2;
      }
      nt.x[nj]=nx+rand()/((double)RAND_MAX+1)*triw*TRI_ADD_XY_DELTA*2-triw*TRI_ADD_XY_DELTA;
      nt.y[nj]=ny+rand()/((double)RAND_MAX+1)*trih*TRI_ADD_XY_DELTA*2-trih*TRI_ADD_XY_DELTA;
      nt.points++;
      if (nt.x[nj]<0)
        nt.x[nj]=0;
      else if (nt.x[nj]>triw)
        nt.x[nj]=triw;
      if (nt.y[nj]<0)
        nt.y[nj]=0;
      else if (nt.y[nj]>trih)
        nt.y[nj]=trih;
    }
    else if ((nt.points>3) && (wastemutations>=(TRI_REMOVE_VERT_LVL+pointstri*TRI_REMOVE_VERT_FACTOR/activetri)) && ((int)(rand()/((double)RAND_MAX+1)*TRI_REMOVE_VERT_RND)==0))
    {
      nj=rand()/((double)RAND_MAX+1)*(nt.points);
      for (j=(nj);j<(nt.points-1);j++)
      {
        nt.x[j]=nt.x[j+1];
        nt.y[j]=nt.y[j+1];
      }
      nt.points--;
    }
    else if ((int)(rand()/((double)RAND_MAX+1)*TRI_WHOLE_RND)==0)
    {
      for (nj=0;nj<TRI_VERT;nj++)
      {
        nt.x[nj]=rand()/((double)RAND_MAX+1)*triw;
        nt.y[nj]=rand()/((double)RAND_MAX+1)*trih;
      }
      nt.r=rand()/((double)RAND_MAX+1)*255;
      nt.g=rand()/((double)RAND_MAX+1)*255;
      nt.b=rand()/((double)RAND_MAX+1)*255;
      nt.a=rand()/((double)RAND_MAX+1)*255;
    }
    else if ((int)(rand()/((double)RAND_MAX+1)*TRI_REMOVE_RND)==0)
    {
      nt.active=0;
    }
    else if ((int)(rand()/((double)RAND_MAX+1)*TRI_MOVE_TRI_RND)==0)
    {
      nk=rand()/((double)RAND_MAX+1)*TRI_NUM;
    }
    else
    {
      if ((int)(rand()/((double)RAND_MAX+1)*nt.points*TRI_XY_C_FACTOR))
      {
        nj=rand()/((double)RAND_MAX+1)*(nt.points);
        if ((int)(rand()/((double)RAND_MAX+1)))
        {
          nt.x[nj]+=rand()/((double)RAND_MAX+1)*triw*TRI_XY_DELTA*2-triw*TRI_XY_DELTA;
          if (nt.x[nj]<0)
            nt.x[nj]=0;
          else if (nt.x[nj]>triw)
            nt.x[nj]=triw;
        }
        else
        {
          nt.y[nj]+=rand()/((double)RAND_MAX+1)*trih*TRI_XY_DELTA*2-trih*TRI_XY_DELTA;
          if (nt.y[nj]<0)
            nt.y[nj]=0;
          else if (nt.y[nj]>trih)
            nt.y[nj]=trih;
        }
      }
      else
      {
        switch ((int)(rand()/((double)RAND_MAX+1)*4))
        {
          case 0:
            nt.r+=rand()/((double)RAND_MAX+1)*TRI_C_DELTA*2-TRI_C_DELTA;
            if (nt.r<0)
              nt.r=0;
            else if (nt.r>255)
              nt.r=255;
            break;
          case 1:
            nt.g+=rand()/((double)RAND_MAX+1)*TRI_C_DELTA*2-TRI_C_DELTA;
            if (nt.g<0)
              nt.g=0;
            else if (nt.g>255)
              nt.g=255;
            break;
          case 2:
            nt.b+=rand()/((double)RAND_MAX+1)*TRI_C_DELTA*2-TRI_C_DELTA;
            if (nt.b<0)
              nt.b=0;
            else if (nt.b>255)
              nt.b=255;
            break;
          case 3:
            nt.a+=rand()/((double)RAND_MAX+1)*TRI_C_DELTA*2-TRI_C_DELTA;
            if (nt.a<0)
              nt.a=0;
            else if (nt.a>255)
              nt.a=255;
            break;
        }
      }
    }
  }
  else if (wastemutations>=(TRI_ADD_TRI_LVL+activetri*TRI_ADD_TRI_FACTOR))
  {
    for (nj=0;nj<TRI_INIT_VERT;nj++)
    {
      nt.x[nj]=rand()/((double)RAND_MAX+1)*triw;
      nt.y[nj]=rand()/((double)RAND_MAX+1)*trih;
    }
    nt.points=TRI_INIT_VERT;
    nt.r=rand()/((double)RAND_MAX+1)*255;
    nt.g=rand()/((double)RAND_MAX+1)*255;
    nt.b=rand()/((double)RAND_MAX+1)*255;
    nt.a=rand()/((double)RAND_MAX+1)*255;
    nt.active=1;
  }
  
  if (!(nt.active==0 && tris[ni].active==0))
  {
    tp=SDL_CreateRGBSurface(SDL_SWSURFACE,triw,trih,24,0,0,0,0);
    SDL_FillRect(tp,NULL,bgcolor);
    for (i=0;i<TRI_NUM;i++)
    {
      t=&tris[i];
      
      if (ni!=nk)
      {
        if (i==ni)
          t=&tris[nk];
        else if (i==nk)
          t=&nt;
      }
      else if (i==ni)
        t=&nt;
      
      if (t->active)
        sge_FilledPolygonAlpha(tp,t->points,t->x,t->y,SDL_MapRGB(screen->format,t->r,t->g,t->b), t->a);
        //filledPolygonRGBA(tp,t->x,t->y,t->points,t->r,t->g,t->b,t->a);
    }
    
    nfit=comp_fitness(tp);
    
    //printf("f: %ld\tnf: %ld\n",fitness,nfit);
    //t=&tris[ni];
    //sge_FilledPolygonAlpha(screen,t->points,t->x,t->y,SDL_MapRGB(screen->format,t->r,t->g,t->b), t->a);
    
    if ((nfit<fitness) || ((nfit==fitness) && ((tris[ni].active==1 && nt.active==0) || (tris[ni].points>nt.points))))
    {
      if (tris[ni].active==0 && nt.active==1)
      {
        activetri++;
        pointstri+=nt.points;
      }
      else if (tris[ni].active==1 && nt.active==0)
      {
        activetri--;
        pointstri-=tris[ni].points;
      }
      else if (tris[ni].points<nt.points)
        pointstri++;
      else if (tris[ni].points>nt.points)
        pointstri--;
      
      if (ni!=nk)
      {
        tris[ni]=tris[nk];
        tris[nk]=nt;
      }
      else
        tris[ni]=nt;
      
      tmp_surface=tripic;
      tripic=tp;
      tp=tmp_surface;
      
      fitness=nfit;
      benefitial++;
      wastemutations=0;
    }
    else
    {
      wastemutations++;
    }
    
    SDL_FreeSurface(tp);
  }
  else
  {
    wastemutations++;
  }
  
  mutations++;
}

void init_objects()
{
  int i;
  TRI *t;
  #if TRI_INIT_RND
    int j;
  #endif
  
  bgcolor=SDL_MapRGB(screen->format,255,255,255);
  SDL_FillRect(tripic,NULL,bgcolor);
  
  init_fitness=comp_fitness(tripic);
  
  for (i=0;i<TRI_NUM;i++)
  {
    t=&tris[i];
    
    #if TRI_INIT_RND
      t->r=rand()/((double)RAND_MAX+1)*255;
      t->g=rand()/((double)RAND_MAX+1)*255;
      t->b=rand()/((double)RAND_MAX+1)*255;
      t->a=rand()/((double)RAND_MAX+1)*255*TRI_INIT_A;
      
      for (j=0;j<TRI_INIT_VERT;j++)
      {
        t->x[j]=rand()/((double)RAND_MAX+1)*triw;
        t->y[j]=rand()/((double)RAND_MAX+1)*trih;
      }
      t->points=TRI_INIT_VERT;
      t->active=1;
      
      sge_FilledPolygonAlpha(tripic,t->points,t->x,t->y,SDL_MapRGB(screen->format,t->r,t->g,t->b), t->a);
    #else
      t->active=0;
    #endif
  }
  
  #if TRI_INIT_RND
    activetri=TRI_NUM;
    pointstri=TRI_NUM*TRI_INIT_VERT;
  #else
    activetri=0;
    pointstri=0;
  #endif
  
  fitness=comp_fitness(tripic);
}

void draw_main()
{
  SDL_Surface *message;
  SDL_Surface *diffpic;
  SDL_Color textColor={0,0,0};
  char text[256];
  SDL_Rect pic_rect={5,5};
  SDL_Rect tripic_rect={triw+10,5};
  double m,s;
  float f,v,hps;
  #if DRAW_DIFF
    SDL_Rect diffpic_rect={triw*2+15,5};
    int x,y;
    Uint8 pr,pg,pb;
    Uint8 tr,tg,tb;
  #endif
  
  Slock(screen);
  sge_Update_OFF();
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  SDL_BlitSurface(pic,NULL,screen,&pic_rect);
  SDL_BlitSurface(tripic,NULL,screen,&tripic_rect);
  
  #if DRAW_DIFF
    diffpic=SDL_CreateRGBSurface(SDL_SWSURFACE,triw,trih,24,0,0,0,0);
    
    for (x=0;x<triw;x++)
    {
      for (y=0;y<trih;y++)
      {
        SDL_GetRGB(GetPixel(pic,x,y),screen->format,&pr,&pg,&pb);
        SDL_GetRGB(GetPixel(tripic,x,y),screen->format,&tr,&tg,&tb);
        
        DrawPixel(diffpic,x,y,abs(pr-tr),abs(pg-tg),abs(pb-tb));
      }
    }
    
    SDL_BlitSurface(diffpic,NULL,screen,&diffpic_rect);
    SDL_FreeSurface(diffpic);
  #endif
  
  if (activetri>0)
    v=(float)pointstri/activetri;
  else
    v=0;
  f=100-(float)fitness/(init_fitness)*100;
  s=modf((double)ticks/(60*1000),&m)*60;
  hps=(float)benefitial/mutations*100;
  sprintf(text,"n:%02d/%02d v:%.1f/%d(%03d) s:%05d/%06d(%04.2f%%) w:%03d f:%05.2f%% t:%02.0f:%04.1lf",activetri,TRI_NUM,v,TRI_VERT,pointstri,benefitial,mutations,hps,wastemutations,f,m,s);
  message=TTF_RenderText_Blended(font, text, textColor );
  apply_surface(10, trih-TEXT_SIZE+30, message, screen );
  SDL_FreeSurface(message);
  
  sge_Update_ON();
  Sulock(screen);
  SDL_Flip(screen);
}

void store_svg()
{
  char filename[256];
  FILE *fPtr;
  int i,j;
  TRI *t;
  int p;
  float f;
  
  f=100-(float)fitness/(init_fitness)*100;
  sprintf(filename,"output_%06d_%02.0f.svg",benefitial,f);
  
  if ((fPtr=fopen(filename,"w"))==NULL)
    return;
  
  fprintf(fPtr,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
  fprintf(fPtr,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n");
  fprintf(fPtr,"\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n\n");
  fprintf(fPtr,"<svg width=\"100%%\" height=\"100%%\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n\n");
  
  fprintf(fPtr,"<polygon points=\"0,0 0,%d %d,%d %d,0\" style=\"fill:rgb(255,255,255); fill-opacity:1.0\"/>\n\n",trih,triw,trih,triw);
  
  p=pause;
  pause=1;
  for (i=0;i<TRI_NUM;i++)
  {
    t=&tris[i];
    if (t->active)
    {
      fprintf(fPtr,"<polygon points=\"");
      for (j=0;j<t->points;j++)
        fprintf(fPtr,"%d,%d ",t->x[j],t->y[j]);
      fprintf(fPtr,"\" style=\"fill:rgb(%d, %d, %d); fill-opacity:%f\"/>\n",t->r,t->g,t->b,((float)t->a)/255);
    }
  }
  pause=p;
  
  fprintf(fPtr,"\n</svg>\n");
  
  fclose(fPtr);
}

void save_state()
{
  FILE *fPtr;
  long tn,vn;
  int p;
  int ret;
  
  printf("saving state...");
  
  if ((fPtr=fopen(STATE_FILE,"wb"))==NULL)
  {
    printf("error: opening file\n");
    return;
  }
  
  tn=TRI_NUM;
  vn=TRI_VERT;
  
  ret=fwrite(&tn,sizeof(tn),1,fPtr);
  ret=fwrite(&vn,sizeof(vn),1,fPtr);
  
  ret=fwrite(&mutations,sizeof(mutations),1,fPtr);
  ret=fwrite(&benefitial,sizeof(benefitial),1,fPtr);
  
  p=pause;
  pause=1;
  ret=fwrite(&tris,sizeof(TRI),TRI_NUM,fPtr);
  pause=p;
  
  printf("done\n");
  
  fclose(fPtr);
}

void load_state()
{
  FILE *fPtr;
  long tn,vn;
  int p;
  int ret;
  TRI *t;
  int i;
  
  printf("loading state...");
  
  if ((fPtr=fopen(STATE_FILE,"rb"))==NULL)
  {
    printf("error: opening file\n");
    return;
  }
  
  ret=fread(&tn,sizeof(tn),1,fPtr);
  ret=fread(&vn,sizeof(vn),1,fPtr);
  
  if (tn==TRI_NUM && vn==TRI_VERT)
  {
    p=pause;
    pause=1;
    
    ret=fread(&mutations,sizeof(mutations),1,fPtr);
    ret=fread(&benefitial,sizeof(benefitial),1,fPtr);
    
    SDL_FillRect(tripic,NULL,SDL_MapRGB(screen->format,255,255,255));
    ret=fread(&tris,sizeof(TRI),TRI_NUM,fPtr);
    activetri=0;
    pointstri=0;
	  wastemutations=0;
    
    for (i=0;i<TRI_NUM;i++)
    {
      t=&tris[i];
      if (t->active)
      {
        activetri++;
        pointstri+=t->points;
        sge_FilledPolygonAlpha(tripic,t->points,t->x,t->y,SDL_MapRGB(screen->format,t->r,t->g,t->b), t->a);
      }
    }
    fitness=comp_fitness(tripic);
    pause=p;
  }
  else
    printf("error: invalid state file\n");
  
  printf("done\n");
  
  fclose(fPtr);
}

void event_handler(SDL_Event event)
{
  SDLKey key;
  
  switch (event.type)
  {
    case SDL_KEYDOWN:
      key=event.key.keysym.sym;
      switch (key)
      {
        case SDLK_ESCAPE:
          running=0;
          break;
        case SDLK_SPACE:
          store_svg();
          break;
        case SDLK_l:
          load_state();
          break;
        case SDLK_s:
          save_state();
          break;
        case SDLK_p:
          pause=!pause;
          break;
        default:
          break;
      }
      break;
    case SDL_QUIT:
      running=0;
      break;
  }
}

#if EVENT_THREAD
int event_handler_thread(void *data)
{
  SDL_Event event;
  
  while (running)
  {
    if(SDL_PollEvent(&event))
    {
      event_handler(event);
    }
    else
    {
      SDL_Delay(1);
    }
  }
  
  return 0;
}
#endif

int main(int argc, char *argv[])
{
  SDL_Surface *tmp_image;
  #if EVENT_THREAD
    SDL_Thread *ev_thread;
  #else
    SDL_Event event;
  #endif
  int tprev=0;
  int i;
  float fps_avg;
  
  if (SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO)<0)
  {
    printf( "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(clean_up);
  
  if(TTF_Init()<0)
  {
    printf( "Unable to init TTF: %s\n", TTF_GetError());
    exit(1);
  }
  
  SDL_WM_SetCaption(TITLE,TITLE);
  
  Uint32 flags = SDL_SWSURFACE|SDL_DOUBLEBUF;
  //SDL_FULLSCREEN SDL_ASYNCBLIT SDL_OPENGL SDL_RESIZABLE
  screen = SDL_SetVideoMode(1, 1, 24, flags);
  if (screen == NULL)
  {
    printf( "Unable to set video mode: %s\n", SDL_GetError());
    exit(1);
  }
  
  //SDL_ShowCursor(SDL_DISABLE);
  
  font=TTF_OpenFont(TEXT_FONT,TEXT_SIZE);
  if (font==NULL)
  {
    printf( "Unable to open font: %s\n", TTF_GetError());
    exit(1);
  }
  
  tmp_image = IMG_Load(PIC_FILENAME);
	if(!tmp_image)
	{
		printf( "Unable to open image\n");
    exit(1);
	}
	pic = SDL_DisplayFormat(tmp_image);
	SDL_FreeSurface(tmp_image);
	
	triw=pic->w;
	trih=pic->h;
	tripic=SDL_CreateRGBSurface(SDL_SWSURFACE,triw,trih,24,0,0,0,0);
	
	#if DRAW_DIFF
	  screen = SDL_SetVideoMode(triw*3+25, trih+40, 24, flags);
	#else
	  screen = SDL_SetVideoMode(triw*2+20, trih+40, 24, flags);
	#endif
  if (screen == NULL)
  {
    printf( "Unable to reset video mode: %s\n", SDL_GetError());
    exit(1);
  }
	
	srand((int)time(0));
	
	mutations=0;
	wastemutations=0;
  benefitial=0;
  fitness=0;
  init_objects();
  
  running=1;
  pause=0;
  
  #if EVENT_THREAD
    ev_thread=SDL_CreateThread(event_handler_thread,NULL);
  #endif
  
  while (running)
  {
    ticks=SDL_GetTicks();
    
    #if !EVENT_THREAD
      while(SDL_PollEvent(&event))
      {
        event_handler(event);
      }
    #endif
    
    if (!pause)
      do_step();
    
    if ((ticks-tprev)>(1000/(MAX_FPS)))
    {
      fps_avg=0;
      for (i=0;i<(TEXT_FPS_ACC-1);i++)
      {
        fps_hist[i]=fps_hist[i+1];
        fps_avg+=fps_hist[i];
      }
      fps_hist[TEXT_FPS_ACC-1]=(1000)/(ticks-tprev);
      fps_avg+=fps_hist[TEXT_FPS_ACC-1];
      fps=fps_avg/TEXT_FPS_ACC;
      
      draw_main();
      tprev=ticks;
    }
    else
    {
      SDL_Delay(0);
    }
  }
  
  #if EVENT_THREAD
    SDL_KillThread(ev_thread);
  #endif
  
  //if (SDL_GetError()) {printf("Err: %s\n", SDL_GetError());}
  
  exit(0);
}

void clean_up()
{
  TTF_CloseFont(font);

  TTF_Quit();
  SDL_Quit();
}


