/*
  ------------------------------------------
  tripic header file
  ------------------------------------------
*/

#ifndef TRIPIC_H
#define TRIPIC_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "SDL.h"
#include "SDL_thread.h"
#include "sge.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "SDL_gfxPrimitives.h"

Uint32 GetPixel(SDL_Surface *surf, int x, int y);
void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B);
void Slock(SDL_Surface *screen);
void Sulock(SDL_Surface *screen);
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination );

void clean_up();

Uint32 inline GetPixel(SDL_Surface *surf, int x, int y)
{
  register int bpp = surf->format->BytesPerPixel;
  register Uint8 *p = (Uint8 *)surf->pixels + y * surf->pitch + x * bpp;

  switch (bpp)
  {
    case 1:
      return *p;

    case 2:
      return *(Uint16 *)p;

    case 3:
      if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
        return p[0] << 16 | p[1] << 8 | p[2];
      else
        return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
      return *(Uint32 *)p;

    default:
      return 0;
  }
}

void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B)
{
  Uint32 color = SDL_MapRGB(screen->format, R, G, B);
  switch (screen->format->BytesPerPixel)
  {
    case 1:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
        *bufp = color;
      }
      break;
    case 2:
      {
        Uint16 *bufp;
        bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
        *bufp = color;
      }
      break;
    case 3:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
        if(SDL_BYTEORDER == SDL_LIL_ENDIAN)
        {
          bufp[0] = color;
          bufp[1] = color >> 8;
          bufp[2] = color >> 16;
        }
        else
        {
          bufp[2] = color;
          bufp[1] = color >> 8;
          bufp[0] = color >> 16;
        }
      }
      break;
    case 4:
      {
        Uint32 *bufp;
        bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
        *bufp = color;
      }
      break;
  }
}

void Slock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    if (SDL_LockSurface(screen)<0)
    {
      return;
    }
  }
}

void Sulock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    SDL_UnlockSurface(screen);
  }
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
  SDL_Rect offset;
  
  offset.x = x;
  offset.y = y;
  
  SDL_BlitSurface( source, NULL, destination, &offset );
}

#endif
